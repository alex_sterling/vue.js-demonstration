var Vue = require('vue');
var VueResource = require('vue-resource');
Vue.use(VueResource);

//pull in component files via vueify
var App = require('./demoComponents/app.vue');

new Vue({
    el: '#vueapp',
    components: {
        app: App
    }
});