<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class LanguagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('languages')->delete();
        $faker = Faker::create('en_US');

        $languages = ['php', 'javascript', 'java', 'c#', 'c++', 'c', 'ruby'];

        foreach($languages as $language){
            App\Language::create([
                'name'      => $language,
                'created_at' => $faker->dateTimeThisYear,
                'updated_at' => $faker->dateTimeThisYear
            ]);
        }
    }
}
