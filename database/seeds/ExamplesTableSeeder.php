<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class ExamplesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('examples')->delete();
        $faker = Faker::create('en_US');

        foreach(range(1,100) as $index){
            $example_set_id = null;
            if(rand(0,1) > 0 ){
                $example_set_id = App\ExampleSet::all()->random()->id;
            }
            $example = App\Example::create([
                'title' => $faker->catchphrase,
                'markdown' => file_get_contents("http://jaspervdj.be/lorem-markdownum/markdown.txt"),
                'username' => App\User::all()->random()->name,
                'upvotes' => rand(-10, 400),
                'example_set_id' => $example_set_id,
                'language_id' => App\Language::all()->random()->id,
                'created_at' => $faker->dateTimeThisYear,
                'updated_at' => $faker->dateTimeThisYear
            ]);

            //Tags

            foreach(range(1, rand(2,12)) as $index){
                //Grab a tag
                $tag = App\Tag::all()->random();

                if(!$example->tags()->find($tag->id)){
                    $example->tags()->attach($tag->id);
                    $example->save();
                }
            }
        }
    }
}
